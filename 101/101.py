from getpass import getpass
import random

print("Witaj w grze papier, kamień, nożyce! Wybierz:\n1 - jeżeli chcesz zagrać z komputerem, \n2 - jeżeli chcesz zagrać z innym graczem.")
try:
    wybor_gry= int(getpass("Twój wybor to? 1 lub 2: "))
except ValueError:
    print("To nie jest liczba")

if wybor_gry == 1:
        print("Wybrałeś grę z komputerem.\n Twój wybór to? Podaj liczbę odpowiadającą znakowi:\n 1 - papier\n 2 - nożyce\n 3 - kamień\n")
        wybor_symbolu_gracza_1 = int(getpass("Twój wybor symbolu to? 1,2,3: "))
        lista_symboli = [1,2,3]
        wybor_symbolu_pc = random.choice(lista_symboli)
        if wybor_symbolu_gracza_1 == wybor_symbolu_pc:
            print("Wybraliście to samo, gramy dalej")
        elif wybor_symbolu_gracza_1 == 1:
            if wybor_symbolu_pc == 2:
                print("Wygrywa pc")
            else:
                print("Wygrywa gracz")
        elif wybor_symbolu_gracza_1 == 2:
            if wybor_symbolu_pc == 3:
                print("Wygrywa pc")
            else:
                print("Wygrywa gracz")
        elif wybor_symbolu_gracza_1 == 3:
            if wybor_symbolu_pc == 2:
                print("Wygrywa pc")
            else:
                print("Wygrywa gracz")
elif wybor_gry == 2:
        print("Wybrałeś grę z innym graczem.\n Każdy z graczy podaj liczbę odpowiadającą znakowi:\n 1 - papier\n 2 - nożyce\n 3 - kamień")
        wybor_symbolu_gracza_1 = int(getpass("Wybor symbolu pierwszego gracza to? 1,2,3: "))
        wybor_symbolu_gracza_2 = int(getpass("Wybor symbolu pierwszego gracza to? 1,2,3: "))
        if wybor_symbolu_gracza_1 == wybor_symbolu_gracza_2:
            print("Wybraliście to samo, gramy dalej")
        elif wybor_symbolu_gracza_1 == 1:
            if wybor_symbolu_gracza_2 == 2:
                print("Wygrywa gracz 2")
            else:
                print("Wygrywa gracz 1")
        elif wybor_symbolu_gracza_1 == 2:
            if wybor_symbolu_gracza_2 == 3:
                print("Wygrywa gracz 2")
            else:
                print("Wygrywa gracz 1")
        elif wybor_symbolu_gracza_1 == 3:
            if wybor_symbolu_gracza_2 == 2:
                print("Wygrywa gracz 2")
            else:
                print("Wygrywa gracz 1")

