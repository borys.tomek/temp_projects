class SantaVillage:
    number_of_buildings = 0
    number_of_people = 0
    number_of_reniffers = 0

    def count_buildings(self):
        SantaVillage.number_of_buildings += 1

    def count_people(self):
        SantaVillage.number_of_people += 1

    def count_reniffers(self):
        SantaVillage.number_of_reniffers += 1


class Elf(SantaVillage):
    def __init__(self, name, id, age):
        self.name = name
        self.id = id
        self.age = age
        super().count_people()

    def about(self):
        print(f"Hi, my name is {self.name}, I'm {self.age} years old. My ID: {self.id}")


class Reniffer(SantaVillage):
    def __init__(self, name, nose_color):
        self.name = name
        self.nose_color = nose_color 
        super().count_reniffers()

    def speak(self):
        return f"Hi! My name is {self.name} and my nose is {self.nose_color}"
class Buildings(SantaVillage):
    def __init__(self, name):
        self.name = name
        super().count_buildings()
    